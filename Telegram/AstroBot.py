import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import json
import requests
import time
import cherrypy

#token: 5013488684:AAFPXUXjq0PV_j_zBNt5LQU0Luzw7mGY_5o

class MyBot:
    def __init__(self,token):
        self.tokenBot = token
        self.bot = telepot.Bot(self.tokenBot)
        MessageLoop(self.bot,{'chat': self.on_chat_message}).run_as_thread()
    
    def on_chat_message(self,msg):
        content_type, chat_type ,chat_ID = telepot.glance(msg)
        message=msg['text']
        if(message == "/start"):
            options = "Ecco i comandi: \nPrendi i valori con /last"
            self.bot.sendMessage(chat_ID,text=options)
        if(message[:5] == "/last"):
            print(message)
            collection = message[6:]
            payload = '{"name":' + '"' + collection + '"}'
            api = 'https://api.listnft.me/api/nft/getnft?q=' + payload
            data = requests.get(api).json()
            last = data['result']['docs'][0]['volumeinfo'][-1]
            volume = str(last['totalVolume'])
            floor = str(last['floorPrice'])
            date = last['time'][:10]
            self.bot.sendMessage(chat_ID,text="Gli ultimi dati sono: \nVolume: " + volume + "\nFloor price: " + floor + "\nAggiornati al " + date)



if __name__ == "__main__":

    token = "5013488684:AAFPXUXjq0PV_j_zBNt5LQU0Luzw7mGY_5o"
    bot = MyBot(token)

    while True:
        time.sleep(3)
