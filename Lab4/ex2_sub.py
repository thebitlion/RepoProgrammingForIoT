from MyMQTT import *
import json
import time
import random

class sensorSub():
    
    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager=MyMQTT(self.clientID,self.broker,self.port,self)

    def start(self):
        self.manager.start()
        self.manager.mySubscribe("IoT_project/#")

    def notify(self, topic, msg):
        payload = json.loads(msg)
        if(topic[-11:] == "temperature"):
            print(f"{topic} measured a temperature of {payload['e'][0]['v']} {payload['e'][0]['u']} at time {payload['e'][0]['t']}")
            return payload['e'][0]['v']
        elif(topic[-8:] == "humidity"):
            print(f"{topic} measured a humidity of {payload['e'][0]['v']} {payload['e'][0]['u']} at time {payload['e'][0]['t']}")
        elif(topic[-9:] == "allSensor"):
            print(payload)

    def stop(self):
        self.manager.stop()

if __name__ == "__main__":
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port = conf["port"]
    test = sensorSub("List3n333r",broker,port)
    test.start()

    while True:
        temp = test
        time.sleep(1)
    
    test.stop()