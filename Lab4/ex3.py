import json
import time
import requests
from queue import Queue
from MyMQTT import *

class sensorSub():

    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager=MyMQTT(self.clientID,self.broker,self.port,self)

    def start(self):
        self.manager.start()
        self.manager.mySubscribe("IoT_project/#")

    def notify(self, topic, msg):
        payload = json.loads(msg)
        if(topic[-8:] == "humidity"):
            q.put(payload['e'][0]['v'])
            print("humidity : %f" % (payload['e'][0]['v']))

    def publish(self, topic, msg):
        self.manager.myPublish(topic,msg)

    def stop(self):
        self.manager.stop()



if __name__ == "__main__":
    q = Queue()
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port = conf["port"]
    listener = sensorSub("List3n333r",broker,port)
    listener.start()
    while True:
        hum = 0
        temp = 0
        for i in range(0,10):
            r = requests.get("http://localhost:8080/temperature")
            result = r.json()
            temp += int(result["temperature"])
            if(not q.empty()):
                hum += int(q.get())
                print("Humidity #%i" % (i))
            if(i == 9):
                print("The average temperature is: %f" % (temp/10))
                print("The average humidity is: %f" % (hum/10))
                time.sleep(3)
    listener.stop()
