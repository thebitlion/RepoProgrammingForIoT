import json
import time
import numpy as np 
from MyMQTT import *

class HBpub():

    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager = MyMQTT(self.clientID,self.broker,self.port,None)

    def start(self):
        self.manager.start()

    def stop(self):
        self.manager.stop()

    def publish(self,topic,msg):
        self.manager.myPublish(topic,msg)

if __name__ == "__main__":
    conf = json.load(open("settings.json"))
    broker = conf['broker']
    port = conf['port']
    pub = HBpub("Pubblish33r", broker, port)
    pub.start()
    time.sleep(10)
    for i in range(24):
        value = float(8*np.random.gamma(2.,2)+55)
        timestamp = int(time.time())
        payload = {"HeartBeat":value,"Time":timestamp}
        pub.publish("IoT_project/HBsensor/HB",payload)
        time.sleep(5)
    pub.stop()


    