import cherrypy
import json
import time
import random

class Rest():

    exposed = True

    def __init__(self):
        pass
    
    @cherrypy.tools.json_out()
    def GET(self, *uri):
        if(str(uri[0]) == "temperature"):
            return json.load(open("measurement.json"))
        elif(str(uri[0]) == "humidity"):
            return json.load(open("settings.json"))
        elif(str(uri[0]) == "allSensor"):
            return json.load(open("settings.json"))
        else:
            return "Try again :("
    
if __name__ == "__main__":
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(Rest(), '/', conf)
    cherrypy.engine.start()
    
    while True:
        temperature = random.randint(20,30)
        outfile = {"temperature":temperature}        
        json.dump(outfile, open("measurement.json","w"))
        time.sleep(5)

    cherrypy.engine.block()