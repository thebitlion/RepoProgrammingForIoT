from MyMQTT import *
import json
import time
import random

class sensorPub():

    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager = MyMQTT(self.clientID,self.broker,self.port,None)

    def publish(self, topic, msg):
        self.manager.myPublish(topic,msg)

    def start(self):
        self.manager.start()

    def stop(self):
        self.manager.stop()

if __name__ == "__main__":
    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    basetopic = conf["baseTopic"]
    sensor1 = sensorPub("Strit3PWR",broker,port)
    sensor1.start()
    while True:
        bn = str(basetopic) + "/sensor1"
        timestamp = time.time()
        temperature = random.randint(25,35)
        humidity = random.randint(40,60)
        data = {
                "bn":bn,
                "e":[{
                        "n":"temperature",
                        "u":"Cel",
                        "t":timestamp,
                        "v":temperature
                },{
                        "n":"humidity",
                        "u":"%",
                        "t":timestamp,
                        "v":humidity
                }]
        }
        bn_t = bn + "/temperature"
        data_t = {
                "bn":bn_t,
                "e":[{
                        "n":"temperature",
                        "u":"Cel",
                        "t":timestamp,
                        "v":temperature   
                }]
        }
        bn_h = bn + "/humidity"
        data_h = {
                "bn":bn_h,
                "e":[{
                        "n":"humidity",
                        "u":"%",
                        "t":timestamp,
                        "v":humidity  
                }]
        }
        sensor1.publish(bn+"/allSensor",data)
        sensor1.publish(bn_t,data_t)
        sensor1.publish(bn_h,data_h)
        time.sleep(1)
    
    sensor1.stop()
    
        
