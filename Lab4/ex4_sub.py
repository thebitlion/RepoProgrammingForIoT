import json
import time
import numpy as np 
from queue import Queue
from MyMQTT import *

class HBsub():

    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager = MyMQTT(self.clientID,self.broker,self.port,self)

    def start(self):
        self.manager.start()
        self.manager.mySubscribe("IoT_project/HBsensor/HB")

    def stop(self):
        self.manager.stop()

    def notify(self, topic, msg):
        payload = json.loads(msg)
        q.put(payload)
        print("Value stored")

if __name__ == "__main__":
    q = Queue()
    values = {"Values":[]}
    conf = json.load(open("settings.json"))
    broker = conf['broker']
    port = conf['port']
    receiver = HBsub("Subbscrib33r", broker, port)
    receiver.start()
    for i in range(24):
        data = q.get()
        values['Values'].append(data)
        print(f"Heartbeat # {i+1} is {data['HeartBeat']}")
        time.sleep(5)
    receiver.stop()
    json.dump(values,open("hrLog.json","w"))




