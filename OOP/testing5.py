from exercise4 import *
from exercise5 import *

if __name__=="__main__":
    #We want to run the script untile the user input a specific command to quit
    #(in this case "q")

    book = AddressBook()

    while True:
        #Putting \ in a string allows us to continue writing in the next line
        user_input=input("Press 's' to show the list of contacts\n\
        Press 'n' to add a contact\n\
        Press 'f' to find a contact\n\
        Press 'd' to delete a contact\n\
        Press 'q' to quit\n")
        if user_input=="s":

            book.show()
            
            pass
        elif user_input=="n":

            name = str(input("What is the name? "))
            surname = str(input("What is the surname? "))
            email = str(input("What is the email? "))

            book.add_contact(name, surname, email)

            print("Contact added!")

            pass
        elif user_input=="f":

            name = str(input("What is the name of the contact you want to find? "))

            book.find_by_name(name)
            
            pass
        elif user_input=="d":

            name = str(input("What is the name of the contact you want to delete? "))

            book.remove_contact(name)

            print("Contact deleted successfully!")
            
            pass
        elif user_input=="q":
            #exit from the loop
            break
        else:
            print("Command not recognized")
        
    print("Goodbye!")
