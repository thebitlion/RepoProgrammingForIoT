import random

class Card:

    def __init__(self, suit=0, value=0):

        self.suit = suit
        self.value = value


class Deck:

    def __init__(self):

        suits = ["Hearts","Diamonds","Clubs","Spades"]
        values = ["A", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"]

        cards = []

        for a in suits:
            for b in values:

                cards.append(Card(a, b))
        
        self.cards = cards

    def shuffle(self):

        random.shuffle(self.cards)
    
    def deal(self, n=1):

        if(len(self.cards)>=n):

            i = 0
            hand = []

            while(i<n):

                hand.append(self.cards.pop())
                i += 1
            
        return hand
            








                
