import math

class Car(object):
    """ We'll exploit encapsulation """

    def __init__(self, name, speed):
        self.__name = name
        self.__speed = speed
        self.__gear = 1
    
    def getName(self):
        return self.__name
    
    def getSpeed(self):
        return self.__speed
    
    def getGear(self):
        return self.__gear
    
    def setSpeed(self, value):
        if(value<=250):
            self.__speed = value
        else:
            self.__speed = 250
    
    def setGear(self, command):
        if(command == 1):
            self.__gear += 1
        elif(command == 0):
            self.__gear -=1
        else:
            print("Command not valid, should be 1 or 0")
    

