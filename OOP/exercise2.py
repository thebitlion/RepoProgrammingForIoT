import math
from exercise1 import *

class Line:

    def __init__(self, m=0, q=0):

        self.m = m
        self.q = q
    
    def line_from_points(self, a, b):

        self.m = (b.y-a.y)/(b.x-a.x)
        self.q = a.x*(b.y-a.y)/(b.x-a.x) + a.y

    def distance(self, point):

        return (self.m*point.x + point.y + self.q)/math.sqrt(self.m**2+1)
    
    def intersection(self, line):

        a = (line.q-self.q)/(self.m-line.m)
        b = self.m*(line.q-self.q)/(self.m-line.m)+self.q

        return Point(a,b)


