import math

class Circle(object):
    """ It is for calculating area and perimeter of a circle """

    def __init__(self, r):
        self.r = r
    
    def perimeter(self):
        return math.pi*self.r*2
    
    def area(self):
        return math.pi*self.r**2

class Cylinder(Circle):

    def __init__(self, r, h):
        Circle.__init__(self, r)
        self.h = h

    def area(self):
        return 2*Circle.area(self)+self.h*Circle.perimeter(self)
    
    def volume(self):
        return self.h*Circle.area(self)
