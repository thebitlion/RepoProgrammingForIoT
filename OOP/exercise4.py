import json

class Contact:

    def __init__(self, name, surname, email):
        self.name = name
        self.surname = surname
        self.email = email
    
    def store(self):

        dic = {"name":self.name,"surname":self.surname,"email":self.email}

        json.dump(dic,open("contacts.json","a"))
