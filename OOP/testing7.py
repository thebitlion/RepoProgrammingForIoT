from exercise7 import *

r1 = int(input("Insert the radius of the circle: "))

circle = Circle(r1)

print("Perimeter = %f \nArea = %f" % (circle.perimeter(), circle.area()))

r2 = int(input("Insert the radius of the cylinder: "))
h = int(input("Insert the height of the cylinder: "))

cylinder = Cylinder(r2, h)

print("Area = %f \nVolume = %f" % (cylinder.area(), cylinder.volume()))