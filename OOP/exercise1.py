import math

class Point:
    """ We use this class to represent 2D points """

    def __init__(self, x, y):

        self.x = x
        self.y = y
    
    def distance(self, point):

        return math.sqrt((point.x-self.x)**2+(point.y-self.y)**2)
    
    def move(self, x, y):

        self.x += x
        self.y += y



    