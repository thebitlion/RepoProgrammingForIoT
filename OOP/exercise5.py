from exercise4 import *

class AddressBook:

    def __init__(self):

        contacts = json.load(open("contacts.json"))

        self.contacts = {"contacts":[]}

        for i in contacts["contacts"]:

            self.contacts["contacts"].append(i)

    def show(self):

        for i in self.contacts["contacts"]:
            print(i)

    def find_by_name(self, name):

        for i in self.contacts["contacts"]:

            if(i["name"]==name):
                print(i)
    
    def remove_contact(self, name):

        for i in self.contacts["contacts"]:

            if(i["name"]==name):
                self.contacts["contacts"].remove(i)
        
        json.dump(self.contacts, open("contacts.json","w"))
    
    def add_contact(self, name, surname, email):

        adding = {"name":name, "surname":surname, "email":email}

        self.contacts["contacts"].append(adding)

        json.dump(self.contacts, open("contacts.json","w"))
        



