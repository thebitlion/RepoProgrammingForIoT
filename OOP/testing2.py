from exercise1 import *
from exercise2 import *

if __name__=="__main__":
    # 1 Simple creation
    l1=Line(m=3,q=2)
    print("l1 has m=%d and q=%d" % (l1.m,l1.q))
    #2 Create line from 2 points
    a=Point(0,1)
    b=Point(2,2)
    l2=Line()
    l2.line_from_points(a,b)
    print("l2 has m=%d and q=%d" % (l2.m,l2.q))
    #3 Function for distance from point and intersection with another line
    l=Line(m=1,q=0)
    a=Point(1,5)
    print("Distance between l and a is %d" % l.distance(a))
    m=Line(-1,2)
    i=l.intersection(m)
    print("Intersection is P(%d , %d)" % (i.x, i.y))
