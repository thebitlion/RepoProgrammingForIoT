from ex1 import *
from ex2 import *
if __name__=="__main__":
	# EX 1 test
	c=Calculator()
	# add
	print(c.add(4,2))
	print(c.add(4,5))
	print(c.add(2,0))
	# sub
	print(c.sub(4,2))
	print(c.sub(4,5))
	print(c.sub(2,0))
	# mul
	print(c.mul(4,2))
	print(c.mul(4,5))
	print(c.mul(2,0))
	# div
	print(c.div(4,2))
	print(c.div(4,5))

###########################
	# EX 2 test
	c=ListCalculator()
	# add
	l1=[1,2,3,4,5]
	l2=[1,2,3,4,0]
	print(c.add(l1))
	print(c.add(l2))
	# sub
	print(c.sub(l1))
	print(c.sub(l2))
	# mul
	print(c.mul(l1))
	print(c.mul(l2))
	# div
	print(c.div(l1))
	print(c.div(l2))

	






