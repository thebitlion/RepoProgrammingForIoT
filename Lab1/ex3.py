import math
import json
import datetime

class CatalogManager(object):

    def __init__(self, catalog):
        self.catalog = json.load(open(catalog))
    
    def searchDeviceByID(self, deviceID):
        for house in self.catalog["houses"]:
            for device in house["devicesList"]:
                if(device["deviceID"]==deviceID):
                    return device
    
    def searchDevicesByHouseID(self, houseID):
        for house in self.catalog["houses"]:
            if(house["houseID"]==houseID):
                return house["devicesList"]
    
    def searchUserByUserID(self, userID):
        for user in self.catalog["usersList"]:
            if(user["userID"]==userID):
                return user

    def searchDevicesByMeasureType(self, measureType):
        for house in self.catalog["houses"]:
            for device in house["devicesList"]:
                for measure in device["measureType"]:
                    if(measure == measureType):
                        return device

    def insertDevice(self, userID, houseID, params):
        condition = True
        deviceID = params["deviceID"]
        params["lastUpdate"]=str(datetime.date.today())
        for house in self.catalog["houses"]:
            if(house["houseID"]==houseID and house["userID"]==userID):
                for i,device in enumerate(house["devicesList"]):
                    if(device["deviceID"]==deviceID):
                        house["devicesList"][i] = params
                        condition = False
                if(condition):
                    house["devicesList"].append(params) 
        return "Updated successfully"
    
    def printAll(self):
        return self.catalog

    def exit(self):
        with open("catalog.json","w") as outfile:
            json.dump(self.catalog, outfile)





