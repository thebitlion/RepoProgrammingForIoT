import math
import json

class ListCalculator(object):

    def __init__(self):
        pass
    
    def add(self, arg_list):
        result = sum(arg_list)
        output = {"input":arg_list,"command":"add","result":result}
        return json.dumps(output)

    def sub(self, arg_list):
        result = arg_list[0]
        for arg in arg_list[1:]:
            result -= arg
        output = {"input":arg_list,"command":"sub","result":result}
        return json.dumps(output)
    
    def mul(self, arg_list):
        result = math.prod(arg_list)
        output = {"input":arg_list,"command":"mul","result":result}
        return json.dumps(output)
    
    def div(self, arg_list):
        result = arg_list[0]
        for arg in arg_list[1:]:
            if(arg==0):
                raise ValueError("The second value should be different than zero")
            else:
                result /= arg
        output = {"input":arg_list,"command":"div","result":result}
        return json.dumps(output)
    
if __name__ == '__main__':

    bol = True
    while bol == True:
        calculator=Calculator()
        comm = input("Please select what to do: \nadd A B \nsub A B \nmul A B\ndiv A B\nexit \n").split()
        calc = comm.pop(0)
        args = []
        for item in comm:
            args.append(float(item))
        if(calc=="add"):
            result = calculator.add(args)
            print(result)
        
        elif(calc=="sub"):
            result = calculator.sub(args)
            print(result)

        elif(calc=="mul"):
            result = calculator.mul(args)
            print(result)

        elif(calc=="div"):
            result = calculator.div(args)
            print(result)

        elif(calc=="exit"):
            break
        else:
            raise ValueError("The operation type is incorrect, write as in the menu")
            




