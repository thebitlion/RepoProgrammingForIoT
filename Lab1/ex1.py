import math
import json

class Calculator(object):

    def __init__(self):
        pass
    
    def add(self, *arg):
        result = sum(arg)
        output = {"input":arg,"command":"add","result":result}
        return json.dumps(output)

    def sub(self, arg1, arg2):
        result = arg1 - arg2
        output = {"input":[arg1,arg2],"command":"sub","result":result}
        return json.dumps(output)
    
    def mul(self, *arg):
        result = math.prod(arg)
        output = {"input":arg,"command":"mul","result":result}
        return json.dumps(output)
    
    def div(self, arg1, arg2):
        if(arg2==0):
            raise ValueError("The second value should be different than zero")
        else:
            result = arg1/arg2
            output = {"input":[arg1,arg2],"command":"sub","result":result}
            return json.dumps(output)
    
if __name__ == '__main__':

    bol = True
    while bol == True:
        calculator=Calculator()
        comm = input("Please select what to do: \nadd A B \nsub A B \nmul A B\ndiv A B\nexit \n").split()
        calc = comm.pop(0)
        if(calc=="add"):
            result = calculator.add(float(comm[0]),float(comm[1]))
            print(result)
        
        elif(calc=="sub"):
            result = calculator.sub(float(comm[0]),float(comm[1]))
            print(result)

        elif(calc=="mul"):
            result = calculator.mul(float(comm[0]),float(comm[1]))
            print(result)

        elif(calc=="div"):
            result = calculator.div(float(comm[0]),float(comm[1]))
            print(result)

        elif(calc=="exit"):
            break
        else:
            raise ValueError("The operation type is incorrect, write as in the menu")
            




