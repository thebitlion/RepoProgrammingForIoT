from ex3 import *
import json

if __name__ == "__main__":
    c = CatalogManager('catalog.json')

    # test searchDeviceByID
    print("\nTest searchDeviceByID:")
    print(c.searchDeviceByID(1))
    print(c.searchDeviceByID(2))
    print(c.searchDeviceByID(3))

    # test searchDevicesByHouseID
    print("\nTest searchDevicesByHouseID:")
    print(c.searchDevicesByHouseID(1))
    print(c.searchDevicesByHouseID(2))
    print(c.searchDevicesByHouseID(3))
    print(c.searchDevicesByHouseID(4))

    # test searchUserByUserID
    print("\nTest searchUserByUserID:")
    print(c.searchUserByUserID(1))
    print(c.searchUserByUserID(2))
    print(c.searchUserByUserID(3))

    # test searchDevicesByMeasureType
    print("\nTest searchDevicesByMeasureType:")
    print(c.searchDevicesByMeasureType("Temperature"))
    print(c.searchDevicesByMeasureType("Humidity"))
    print(c.searchDevicesByMeasureType("Proximity"))
    print(c.searchDevicesByMeasureType("Light"))

    # test insertDevice
    # new device
    print("\nTest insertDevice (new):")
    newDevice = {
          "deviceID": 4,
          "deviceName": "DHT11",
          "measureType": ["Temperature", ],
          "availableServices": ["REST"],
          "servicesDetails": [
            {
              "serviceType": "REST",
              "serviceIP": "192.1.1.1:8080"
            }
          ],
        }
    print(c.insertDevice(1, 1, newDevice))

    # update device
    print("\nTest insertDevice (update):")
    newDevice = {
          "deviceID": 1,
          "deviceName": "DHT11",
          "measureType": ["Temperature", ],
          "availableServices": ["REST"],
          "servicesDetails": [
            {
              "serviceType": "REST",
              "serviceIP": "192.1.1.1:8080"
            }
          ],
        }
    print(c.insertDevice(1, 1, newDevice))


    # test printAll
    print(c.printAll())

    #saving in new file
    c.exit()