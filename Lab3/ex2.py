import math
import json
import datetime
import cherrypy

class CatalogManager(object):

    def __init__(self, catalog):
        self.catalog = json.load(open(catalog))
    
    def searchDeviceByID(self, deviceID):
        for house in self.catalog["houses"]:
            for device in house["devicesList"]:
                if(device["deviceID"]==deviceID):
                    return device
    
    def searchDevicesByHouseID(self, houseID):
        for house in self.catalog["houses"]:
            if(house["houseID"]==houseID):
                return house["devicesList"]
    
    def searchUserByUserID(self, userID):
        for user in self.catalog["usersList"]:
            if(user["userID"]==userID):
                return user

    def searchDevicesByMeasureType(self, measureType):
        for house in self.catalog["houses"]:
            for device in house["devicesList"]:
                for measure in device["measureType"]:
                    if(measure == measureType):
                        return device

    def insertDevice(self, userID, houseID, params):
        condition = True
        deviceID = params["deviceID"]
        params["lastUpdate"]=str(datetime.date.today())
        for house in self.catalog["houses"]:
            if(house["houseID"]==houseID and house["userID"]==userID):
                for i,device in enumerate(house["devicesList"]):
                    if(device["deviceID"]==deviceID):
                        house["devicesList"][i] = params
                        condition = False
                if(condition):
                    house["devicesList"].append(params) 
        return "Updated successfully"
    
    def printAll(self):
        return self.catalog

    def exit(self):
        with open("catalog.json","w") as outfile:
            json.dump(self.catalog, outfile)

class SearchDevice(CatalogManager):

    exposed = True

    def __init__(self, catalog):
        CatalogManager.__init__(self, catalog)


    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def GET(self, **params):
        result = {}
        data = []
        for key in params.keys():
            data.append(params[key])
        if(data[0]=="deviceByID"):
            return CatalogManager.searchDeviceByID(self,int(data[1]))
        elif(data[0]=="deviceByHouse"):
            return CatalogManager.searchDevicesByHouseID(self,int(data[1]))
        elif(data[0]=="userByID"):
            return CatalogManager.searchUserByUserID(self,int(data[1]))
        elif(data[0]=="deviceByMeasure"):
            return CatalogManager.searchDevicesByMeasureType(self,data[1])
        elif(data[0]=="printAll"):
            return self.catalog
    
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def POST(self):
        params = cherrypy.request.json
        userID = params.pop("userID")
        houseID = params.pop("houseID")
        CatalogManager.insertDevice(self, userID, houseID, params)
        with open("catalog.json", "w") as outfile:
            json.dump(self.catalog, outfile)
        return self.catalog
        

if __name__ == '__main__':
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(SearchDevice("catalog.json"), '/catalog', conf)
    # this is needed if you want to have the custom error page
    # cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()
    cherrypy.engine.block()

