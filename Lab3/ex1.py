import math
import json
import cherrypy

class Calculator(object):

    def __init__(self):
        pass
    
    def add(self, arg_list):
        result = sum(arg_list)
        output = {"input":arg_list,"command":"add","result":result}
        return json.dumps(output)

    def sub(self, arg_list):
        result = arg_list[0]
        for arg in arg_list[1:]:
            result -= arg
        output = {"input":arg_list,"command":"sub","result":result}
        return json.dumps(output)
    
    def mul(self, arg_list):
        result = math.prod(arg_list)
        output = {"input":arg_list,"command":"mul","result":result}
        return json.dumps(output)
    
    def div(self, arg_list):
        result = arg_list[0]
        for arg in arg_list[1:]:
            if(arg==0):
                raise ValueError("The second value should be different than zero")
            else:
                result /= arg
        output = {"input":arg_list,"command":"div","result":result}
        return json.dumps(output)

class APIadd(Calculator):

    exposed = True

    def __init__(self):
        pass
    
    def GET(self, **params):
        operators = []
        for key in params.keys():
            operators.append(float(params[key]))
        result = operators[0] + operators[1]
        return Calculator.add(self, operators)

class APIsub(Calculator):

    exposed = True

    def __init__(self):
        pass
    
    def GET(self, **params):
        operators = []
        for key in params.keys():
            operators.append(float(params[key]))
        return Calculator.sub(self, operators)

class APImul(Calculator):

    exposed = True

    def __init__(self):
        pass
    
    def GET(self, **params):
        operators = []
        for key in params.keys():
            operators.append(float(params[key]))
        return Calculator.mul(self, operators)

class APIdiv(Calculator):

    exposed = True

    def __init__(self):
        pass
    
    def GET(self, **params):
        operators = []
        for key in params.keys():
            operators.append(float(params[key]))
        return Calculator.div(self, operators)


if __name__ == '__main__':
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(APIadd(), '/add', conf)
    cherrypy.tree.mount(APIsub(), '/sub', conf)
    cherrypy.tree.mount(APImul(), '/mul', conf)
    cherrypy.tree.mount(APIdiv(), '/div', conf)
    # this is needed if you want to have the custom error page
    # cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()
    cherrypy.engine.block()

    
            




