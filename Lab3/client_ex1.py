import json
import requests

while True:
    operation = input("Please select the operation to be done by writing: \nadd | mul | sub | div \n")
    op1 = int(input("Write the first number: "))
    op2 = int(input("Write the second number: "))

    payload = {"op1":op1,"op2":op2}
    
    if(operation=="add"):
        r = requests.get("http://localhost:8080/add", params=payload)
        result = r.json()
        print("Your operation is %s \nAnd the result is %f" % (result["command"],result["result"]))
    elif(operation=="sub"):
        r = requests.get("http://localhost:8080/sub", params=payload)
        result = r.json()
        print("Your operation is %s \nAnd the result is %f" % (result["command"],result["result"]))
    elif(operation=="mul"):
        r = requests.get("http://localhost:8080/mul", params=payload)
        result = r.json()
        print("Your operation is %s \nAnd the result is %f" % (result["command"],result["result"]))
    elif(operation=="div"):
        r = requests.get("http://localhost:8080/div", params=payload)
        result = r.json()
        print("Your operation is %s \nAnd the result is %f" % (result["command"],result["result"]))

    if(input("Press q to quit ")=="q"):
        break

