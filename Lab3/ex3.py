import math
import json
import cherrypy
import requests

class Slots(object):

    exposed = True

    def __init__(self):
        pass

    @cherrypy.tools.json_out()
    def GET(self, **params):
        values = []
        for key in params.keys():
            if(params[key]!=""):
                values.append(int(params[key]))
            else:
                values.append(10)
        r = requests.get("https://www.bicing.barcelona/en/get-stations")
        data = r.json()
        stations = data["stations"][0:values[0]]
        def funStations(station):
            return station.get("slots")
        stations.sort(key=funStations)
        if(values[1]!=10):
            return stations
        else:
            stations.reverse()
            return stations

class Bikes(object):

    exposed = True

    def __init__(self):
        pass

    @cherrypy.tools.json_out()
    def GET(self, **params):
        values = []
        for key in params.keys():
            if(params[key]!=""):
                values.append(int(params[key]))
            else:
                values.append(10)
        r = requests.get("https://www.bicing.barcelona/en/get-stations")
        data = r.json()
        stations = data["stations"][0:values[0]]
        def funStations(station):
            return station.get("bikes")
        stations.sort(key=funStations)
        if(values[1]!=10):
            return stations
        else:
            stations.reverse()
            return stations

class BikeSlots(object):

    exposed = True

    def __init__(self):
        pass

    @cherrypy.tools.json_out()
    def GET(self, **params):
        values = []
        for key in params.keys():
            values.append(int(params[key]))
        r = requests.get("https://www.bicing.barcelona/en/get-stations")
        data = r.json()
        stations = []
        for station in data["stations"]:
            if(station["electrical_bikes"]>values[0] and station["slots"]>values[1]):
                stations.append(station)       
        
        return stations

class Count(object):

    exposed = True

    def __init__(self):
        pass

    @cherrypy.tools.json_out()
    def GET(self):
        r = requests.get("https://www.bicing.barcelona/en/get-stations")
        data = r.json()
        bikes = 0
        slots = 0
        for station in data["stations"]:
            bikes += station["bikes"]
            slots += station["slots"]    
        
        return {"bikes":bikes,"slots":slots}

if __name__ == '__main__':
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(Slots(), '/slots', conf)
    cherrypy.tree.mount(Bikes(), '/bikes', conf)
    cherrypy.tree.mount(BikeSlots(), '/bikes-slots', conf)
    cherrypy.tree.mount(Count(), '/', conf)
    # this is needed if you want to have the custom error page
    # cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()
    cherrypy.engine.block()

