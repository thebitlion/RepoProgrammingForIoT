import requests
import json

while True:
    operation = input("Please select what to do: \na to search a device by ID \nb to search devices by house ID \nc to search a user by ID \n\
    d to search by measure type \ne to print the whole catalog \nf to add a device \nq to exit \n")

    if(operation=="a"):
        userID = int(input("Insert the ID you want to search: "))
        payload = {"op1":"deviceByID","op2":userID}
        r = requests.get("http://localhost:8080/catalog", params=payload)
        result = r.json()
        print(result)
    elif(operation=="b"):
        houseID = int(input("Insert the ID you want to search: "))
        payload = {"op1":"deviceByHouse","op2":houseID}
        r = requests.get("http://localhost:8080/catalog", params=payload)
        result = r.json()
        print(result)
    elif(operation=="c"):
        userID = int(input("Insert the ID you want to search: "))
        payload = {"op1":"userByID","op2":userID}
        r = requests.get("http://localhost:8080/catalog", params=payload)
        result = r.json()
        print(result)        
    elif(operation=="d"):
        measure = input("Insert the measure type you want to search: ")
        payload = {"op1":"deviceByMeasure","op2":measure}
        r = requests.get("http://localhost:8080/catalog", params=payload)
        result = r.json()
        print(result)
    elif(operation=="e"):
        payload = {"op1":"printAll","op2":0}
        r = requests.get("http://localhost:8080/catalog", params=payload)
        result = r.json()
        print(result)
    elif(operation=="f"):
        payload = {}
        payload["userID"] = int(input("What's the userID? "))
        payload["houseID"] = int(input("What's the houseID? "))
        payload["deviceID"] = int(input("Device ID? "))
        payload["deviceName"] = input("Device Name? ")
        payload["measureType"] = []
        while True:
            payload["measureType"].append(input("Write the measureType: "))
            if(input("Want to add another? Y N ")=="N"):
                break
        payload["availableServices"] = []
        payload["serviceDetails"] = []
        while True:
            payload["availableServices"].append(input("Write an available Service: "))
            i = len(payload["availableServices"])
            service = payload["availableServices"][i-1]
            IP = input("What is its IP? ")
            available = {"serviceType":service,"serviceIP":IP}
            payload["serviceDetails"].append(available)
            if(input("Want to add another? Y N ")=="N"):
                break
        r = requests.post("http://localhost:8080/catalog", json=payload)
        result = r.json()
        print(result)
    elif(operation=="q"):
        break