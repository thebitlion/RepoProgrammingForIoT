import cherrypy
import json
import time
import random
import requests
from MyMQTT import *

class device1Pub():

    def __init__(self, clientID, broker, port):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.manager = MyMQTT(self.clientID,self.broker,self.port,None)
    
    def start(self):
        self.manager.start()
    
    def stop(self):
        self.manager.stop()
    
    def publish(self,topic,msg):
        self.manager.myPublish(topic,msg)
        print(f"Published on {topic}")
    
if __name__ == "__main__":
    clientID = "D3vic3One!"
    while True:
        r = requests.get("http://localhost:8080/catalog?op=3&id=1")
        if(r.json()=="Not found"):
            requests.post("http://localhost:8080/catalog/newdevice",json=json.load(open("device1.json")))
            print("Registered new!")
        else:
            requests.post("http://localhost:8080/catalog/uptdevice",json=json.load(open("device1.json")))
            print("Registered!")
        
        r = requests.get("http://localhost:8080/catalog?op=1")
        mqtt = r.json()
        print(mqtt)
        device1 = device1Pub(clientID,str(mqtt['broker']),int(mqtt['port']))
        device1.start()

        temperature = random.randint(20,30)
        humidity = random.randint(40,60)
        measurement = json.load(open("measurement.json"))
        for measure in measurement['e']:
            if(measure['n']=="temperature"):
                measure["v"] = temperature
                measure["t"] = int(time.time())
            elif(measure['n']=="humidity"):
                measure["v"] = humidity
                measure["t"] = int(time.time())
        json.dump(measurement, open("measurement.json","w"))
        
        device1.publish("IoT_project/device1",measurement)
        device1.stop()
        time.sleep(60)
