import json
import time
import cherrypy

class CatalogManager(object):

    def __init__(self, catalog):
        self.catalog = json.load(open(catalog))

    def searchBroker(self):
        broker = self.catalog['brokerMQTT']
        port = self.catalog['portMQTT']
        return {"broker":broker,"port":port}

    def insertDevice(self, params):
        condition = True
        deviceID = params["deviceID"]
        for device in self.catalog['devices']:
            if(device['deviceID']==deviceID):
                condition = False
                return "There is already a device with this ID"
        if(condition):
            params['timestamp'] = int(time.time())
            self.catalog['devices'].append(params)
            return "Inserted successfully"
    
    def updateDevice(self, params):
        condition = True
        deviceID = params["deviceID"]
        for i,device in enumerate(self.catalog['devices']):
            if(device['deviceID']==deviceID):
                condition = False
                params['timestamp'] = int(time.time())
                self.catalog['devices'][i] = params
                return "Updated successfully"
        if(condition):
            return "There is no device with this ID"
    
    def retrieveDevices(self):
        devices = []
        for device in self.catalog['devices']:
            devices.append(device)
        return devices
    
    def retrieveDevice(self, deviceID):
        for device in self.catalog['devices']:
            if(device['deviceID']==deviceID):
                return device
    
    def registerUser(self, params):
        condition = True
        userID = params['userID']
        for user in self.catalog['users']:
            if(user['userID']==userID):
                condition = False
                return "There is already a user with this ID"
        if(condition):
            self.catalog['users'].append(params)
            return "Registered successfully"
    
    def retrieveUsers(self):
        users = []
        for user in self.catalog['users']:
            users.append(user)
        return users
    
    def retrieveUser(self, userID):
        for user in self.catalog['users']:
            if(user['userID']==userID):
                return user
    
    def deleteDevices(self):
        condition = False
        time_now = int(time.time())
        for i,device in enumerate(self.catalog['devices']):
            if(time_now-device['timestamp'] >= 120):
                self.catalog['devices'].pop(i)
                condition = True
        if(condition):
            with open("catalog.json","w") as outfile:
                json.dump(self.catalog, outfile)
            return "Devices removed"
        else:
            return "No device to remove"



class CatalogREST(CatalogManager):

    exposed = True

    def __init__(self, catalog):
        CatalogManager.__init__(self,catalog)
    
    @cherrypy.tools.json_out()
    def GET(self,**params):
        data = []
        for key in params.keys():
            data.append(params[key])
        #1 info about the broker for MQTT (IP address and port)
        if(int(data[0])==1):
            return CatalogManager.searchBroker(self)
        #2 all registered devices
        if(int(data[0])==2):
            return CatalogManager.retrieveDevices(self)
        #3 a specific device
        if(int(data[0])==3):
            result = CatalogManager.retrieveDevice(self,int(data[1]))
            if(result == None):
                return "Not found"
            else:
                return result
        #4 all users
        if(int(data[0])==4):
            return CatalogManager.retrieveUsers(self)
        #5 a specific user
        if(int(data[0])==5):
            result = CatalogManager.retrieveUser(self,int(data[1]))
            if(result == None):
                return "Not found"
            else:
                return result
    
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def POST(self, *uri):
        params = cherrypy.request.json
        #1 add a device
        if(uri[0]=="newdevice"):
            result = CatalogManager.insertDevice(self,params)
        #2 update device
        if(uri[0]=="uptdevice"):
            result = CatalogManager.updateDevice(self,params)
        #3 add a user
        if(uri[0]=="newuser"):
            result = CatalogManager.registerUser(self,params)
        with open("catalog.json","w") as outfile:
            json.dump(self.catalog, outfile)
        return result

if __name__ == "__main__":
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(CatalogREST("catalog.json"), '/catalog', conf)
    cherrypy.engine.start()
    while True:
        manager = CatalogManager("catalog.json")
        time.sleep(120)
        print(manager.deleteDevices())
    cherrypy.engine.block()