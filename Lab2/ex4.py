import math
import json
import cherrypy

class Frontend(object):

    exposed = True

    def __init__(self):
        self.id = 1
    
    def GET(self):
        return open("index.html")
    
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def POST(self):
        params = cherrypy.request.json
        devices = json.load(open("devices.json"))
        devices["devicesList"].append(params)
        with open("devices.json", "w") as outfile:
            json.dump(devices, outfile)
        return json.dumps(devices)


class Devices(object):

    exposed = True

    def __init__(self):
        self.id = 2
    
    def GET(self):
        return open("devices.json")





if __name__ == '__main__':
    conf = {
        '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on': True
        }
    }
    cherrypy.tree.mount(Frontend(), '/', conf)
    cherrypy.tree.mount(Devices(), '/devicesList', conf)
    # this is needed if you want to have the custom error page
    # cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()
    cherrypy.engine.block()

    
            




