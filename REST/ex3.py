import json
import requests

#I'll use coinstats public API to obtain some data

bol = True
while bol == True:
    dec = input("Available commands: \na = search best coins list \nb = quit \n")
    if dec == "a":
        limit = int(input("Define the number of coins you want to see: "))
        skip = int(input("Define the number of coins to skip: "))

        payload = {"skip":skip, "limit":limit}

        r = requests.get("https://api.coinstats.app/public/v1/coins", params=payload)

        print(r.text)

        result = json.dumps(r.json())

        f = open("result.json", "w")
        f.write(result)
        f.close

    if dec == "b":
        break
